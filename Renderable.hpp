#ifndef _psycho_renderable_hpp_
#define _psycho_renderable_hpp_

/*! \brief Describes an ncurses renderable object.
 */

class Renderable {
	private:
	public:

		/*! \brief Gets the character at a specific position.
		 *  \arg \c y The Y value.
		 *  \arg \c x The X value.
		 *  \return The character.
		 */
		virtual chtype charAt(int, int)=0;

		/*! \brief Initializes the ncurses objects.
		 */
		virtual void init()=0;

		/*! \brief Renders the ncurses objects.
		 */
		virtual void render()=0;

		/*! \brief Tests whether the specified position is blocked.
		 *  \arg \c y The Y value.
		 *  \arg \c x The X value.
		 *  \return If the position is blocked.
		 */
		virtual bool testBlocked(int, int)=0;
};

#endif

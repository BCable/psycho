CXX = g++
CFLAGS = -Wall -lncurses

psycho: psycho.o Character.o Coords.o Environment.o Floor.o GameCore.o GameEngine.o Inventory.o Item.o ItemMap.o Memory.o Palette.o Room.o Vision.o
	${CXX} ${CFLAGS} -o psycho psycho.o Character.o Coords.o Environment.o Floor.o GameCore.o GameEngine.o Inventory.o Item.o ItemMap.o Memory.o Palette.o Room.o Vision.o

psycho.o: psycho.cpp
	${CXX} ${CFLAGS} -c psycho.cpp

Character.o: Character.cpp
	${CXX} ${CFLAGS} -c Character.cpp

Coords.o: Coords.cpp
	${CXX} ${CFLAGS} -c Coords.cpp

Environment.o: Environment.cpp
	${CXX} ${CFLAGS} -c Environment.cpp

Floor.o: Floor.cpp
	${CXX} ${CFLAGS} -c Floor.cpp

GameCore.o: GameCore.cpp
	${CXX} ${CFLAGS} -c GameCore.cpp

GameEngine.o: GameEngine.cpp
	${CXX} ${CFLAGS} -c GameEngine.cpp

Inventory.o: Inventory.cpp
	${CXX} ${CFLAGS} -c Inventory.cpp

Item.o: Item.cpp
	${CXX} ${CFLAGS} -c Item.cpp

ItemMap.o: ItemMap.cpp
	${CXX} ${CFLAGS} -c ItemMap.cpp

Memory.o: Memory.cpp
	${CXX} ${CFLAGS} -c Memory.cpp

Palette.o: Palette.cpp
	${CXX} ${CFLAGS} -c Palette.cpp

Room.o: Room.cpp
	${CXX} ${CFLAGS} -c Room.cpp

Vision.o: Vision.cpp
	${CXX} ${CFLAGS} -c Vision.cpp

clean:
	rm -f psycho.o Character.o Coords.o Environment.o Floor.o GameCore.o GameEngine.o Inventory.o Item.o ItemMap.o Memory.o Palette.o Room.o Vision.o
	rm -f psycho

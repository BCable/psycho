#include "Memory.hpp"

// constructors {{{

/*! \brief Default constructor.
 *  \arg \c turns The amount of turns associated with the memory.
 */
Memory::Memory(short turns){
	this->turns=turns;
	this->turnsUsed=0;
}

// }}}

// private methods {{{

bool Memory::coordsInMemory(int y, int x){
	list<Coords*>::iterator it;
	bool                    ret;

	ret=false;
	for(it=turnMemories.begin(); it!=turnMemories.end(); it++){
		if((*it)->getY()==y && (*it)->getX()==x){
			ret=true;
			break;
		}
	}

	return ret;
}

// }}}

// public methods {{{

/*! \brief Gets the turns.
 *  \return The turns.
 */
short Memory::getTurns(){
	return turns;
}

/*! \brief Records a new memory entry.
 *  \arg \c y The Y value of the memory.
 *  \arg \c x The X value of the memory.
 */
void Memory::recordEntry(int y, int x){
	turnCounts.push_back(turns-turnsUsed);
	turnMemories.push_back(new Coords(y, x));
	turnsUsed=turns;
}

/*! \brief Takes a turn off, and forgets memories that are too old.
 */
void Memory::takeTurn(){
	list<short>::iterator   itC;
	list<Coords*>::iterator itME;
	int                     x;
	int                     y;

	if(turnCounts.size()==0)
		return;

	itC=turnCounts.begin();
	(*itC)--;

	while(
		turnCounts.size()!=0 &&
		(*(itC=turnCounts.begin()))!=-1 &&
		(*itC)==0
	){
		turnCounts.pop_front();
		itME=turnMemories.begin();
		y=(*itME)->getY();
		x=(*itME)->getX();
		delete (*itME);

		if(!coordsInMemory(y, x))
			mvaddch(y, x, ' ');

		turnMemories.pop_front();
	}

	turnsUsed--;
}

// }}}

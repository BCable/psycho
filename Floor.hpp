#ifndef _psycho_floor_hpp_
#define _psycho_floor_hpp_

#include <curses.h>

#include <vector>

#include "Renderable.hpp"
#include "Room.hpp"

using namespace std;

/*! \brief Collection of Room objects.
 *
 * Describes a collection of Room objects that need to be associated with an
 * Environment.
 */

class Floor: public Renderable {
	private:
		vector<Room*> rooms;

	public:
		// constructors
		Floor();

		// inherited methods
		chtype charAt(int, int);
		void   init();
		void   render();

		// public methods
		void addRoom(Room*);
		bool testBlocked(int, int);
};

#endif

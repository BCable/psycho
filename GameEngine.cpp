#include "GameEngine.hpp"

#include <curses.h>
// debug
#include <stdlib.h>

#include "Floor.hpp"
#include "GameCore.hpp"
#include "Item.hpp"

// constructors {{{

/*! \brief Default constructor.
 */
GameEngine::GameEngine(){
}

// }}}

// singleton methods {{{

GameEngine* GameEngine::geInstance;

/*! \brief Gets the singleton instance of the class.
 *  \return The singleton instance.
 */
GameEngine* GameEngine::getInstance(){
	if(!geInstance)
		geInstance=new GameEngine;
	return geInstance;
}

// }}}

// private methods {{{

void GameEngine::ncurses_init(){
	initscr();
	cbreak();
	noecho();
	if(has_colors()){
		start_color();
		use_default_colors();
	}
}

// }}}

// public methods {{{

/*! \brief Gets the Character.
 *  \return The Character.
 */
Character* GameEngine::getCharacter(){
	return c;
}

/*! \brief Gets the Environment.
 *  \return The Environment.
 */
Environment* GameEngine::getCurEnviron(){
	return e;
}

/*! \brief Initializes the engine.
 */
void GameEngine::init(){
	Floor* f;
	Item*  i;
	Room*  r;

	ncurses_init();

	f=new Floor;
	e=new Environment(f);

	i=new Item('b' | GC->palette->RED | GC->palette->BOLD, false);
	e->placeItem(i, 20, 20);

	r=new Room(5, 15, 3, 11, Room::WALLS_OPEN_BOTTOM);
	f->addRoom(r);
	f->addRoom(new Room(15, 9, 8, 3, Room::WALLS_OPEN_RIGHT));
	f->addRoom(new Room(5, 15, 23, 11, Room::WALLS_OPEN_TOP));
	f->addRoom(new Room(15, 9, 8, 25, Room::WALLS_OPEN_LEFT));
	f->init();

	c=new Character(r->getY()+2, r->getX()+2);
	c->init();
}

/*! \brief Main execution loop.
 */
void GameEngine::main(){
	bool   breakout;
	Item*  i;
	chtype ic;
	bool   user_hitwall;
	bool   user_poschanged;
	int    user_newposx;
	int    user_newposy;


	breakout=false;
	user_poschanged=false;
	for(;;){
		if(user_poschanged){
			c->move(user_newposy, user_newposx);
			c->takeTurn();
			user_poschanged=false;
		}
		ic=wgetch(c->getWindow());

		user_newposy=c->getY();
		user_newposx=c->getX();

		switch(ic){
			// quit
			case 'q':
				breakout=true;
				break;

			// movement
			case 'h':
				user_newposx--;
				break;
			case 'j':
				user_newposy++;
				break;
			case 'k':
				user_newposy--;
				break;
			case 'l':
				user_newposx++;
				break;

			// movement diagonal
			case 'y':
				user_newposy--;
				user_newposx--;
				break;
			case 'u':
				user_newposy--;
				user_newposx++;
				break;
			case 'b':
				user_newposy++;
				user_newposx--;
				break;
			case 'n':
				user_newposy++;
				user_newposx++;
				break;

			// pickup item
			case ',':
				i=e->takeItemAt(c->getY(), c->getX());

				if(i!=NULL)
					c->giveItem(i);

				break;

			// wait
			case '.':
				c->takeTurn();
				break;
		}

		if(breakout)
			break;

		user_hitwall=e->testBlocked(user_newposy, user_newposx);

		if(!user_hitwall)
			user_poschanged=true;

	}
}

/*! \brief Starts the application.
 */
void GameEngine::start(){
	init();
	main();
}

// }}}

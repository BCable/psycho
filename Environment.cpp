#include "Environment.hpp"

// constructors {{{

/*! \brief Default constructor.
 *  \arg \c f The initial Floor to deal with.
 */
Environment::Environment(Floor* f){
	this->f=f;
	this->im=new ItemMap;
}

// }}}

// public methods {{{

/*! \brief Gets the character at a specific position.
 *  \arg \c y The Y value.
 *  \arg \c x The X value.
 *  \return The character.
 */
chtype Environment::charAt(int y, int x){
	Item*  i;
	chtype ret;

	if((i=im->itemAt(y, x))!=NULL)
		ret=i->getChar();
	else
		ret=f->charAt(y, x);

	return ret;
}

/*! \brief Gets the Floor.
 *  \return The Floor.
 */
Floor* Environment::getFloor(){
	return f;
}

/*! \brief Places a new Item in the Environment.
 *  \arg \c i The Item.
 *  \arg \c y The Y value.
 *  \arg \c x The X value.
 */
void Environment::placeItem(Item* i, int y, int x){
	im->placeItem(i, y, x);
}

/*! \brief Removes and returns an item at a specific position.
 *  \arg \c y The Y value.
 *  \arg \c x The X value.
 *  \return The Item.
 */
Item* Environment::takeItemAt(int y, int x){
	return im->takeItemAt(y, x);
}

/*! \brief Tests if a specific position is blocked from being accessible.
 *  \arg \c y The Y value.
 *  \arg \c x The X value.
 *  \return If that position is blocked.
 */
bool Environment::testBlocked(int y, int x){
	return f->testBlocked(y, x);
}

// }}}

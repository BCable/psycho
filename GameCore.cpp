#include "GameCore.hpp"

// construactors {{{

/*! \brief Default constructor.
 */

GameCore::GameCore(){
	palette=new Palette;
}

// }}}

// singleton methods {{{

GameCore* GameCore::gcInstance;

/*! \brief Gets the singleton instance of this class.
 *  \return The singleton instance.
 */
GameCore* GameCore::getInstance(){
	if(!gcInstance)
		gcInstance=new GameCore;
	return gcInstance;
}

// }}}

#ifndef _psycho_coords_hpp_
#define _psycho_coords_hpp_

/*! \brief Simple coordinates object.
 */

class Coords {
	private:
		int x;
		int y;

	public:
		Coords(int, int);

		int getX();
		int getY();
};

#endif

#ifndef _psycho_itemmap_hpp_
#define _psycho_itemmap_hpp_

#include <list>

#include "Coords.hpp"
#include "Item.hpp"

using namespace std;

/*! \brief Stores pairs of Item and Coords objects for use in an Environment.
 */

class ItemMap {
	private:
		/*! \brief Describes a set of results when looking for certain Item
		 *         objects.
		 */
		struct rsItem {
			Coords* coords;
			Item*   item;
		};

		list<Coords*> coords;
		list<Item*>   items;

		rsItem rsItemAt(int, int);

	public:
		ItemMap();

		Item* itemAt(int, int);
		void  placeItem(Item*, int, int);
		Item* takeItemAt(int, int);
};

#endif

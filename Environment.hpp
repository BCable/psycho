#ifndef _psycho_environment_hpp_
#define _psycho_environment_hpp_

#include <curses.h>

#include "Floor.hpp"
#include "Item.hpp"
#include "ItemMap.hpp"

/*! \brief Stores the environment that the Character resides in.
 *
 * Stores the Floor and ItemMap for the Character to have access to.
 */

class Environment {
	private:
		Floor*   f;
		ItemMap* im;

	public:
		Environment(Floor*);

		chtype charAt(int, int);
		Floor* getFloor();
		void   placeItem(Item*, int, int);
		Item*  takeItemAt(int, int);
		bool   testBlocked(int, int);
};

#endif

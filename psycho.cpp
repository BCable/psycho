#include "GameEngine.hpp"

/*! \file psycho.cpp
 *  \brief The main execution file.
 */

/*! \fn int main
 *  \brief The main function.
 *  \arg \c argc If you don't know what this is, please don't touch this code.
 *  \arg \c argv No seriously, please go away now.
 *  \return Returns zero.  Always.  Errors are not possible with this perfect
 *          code.
 */

int main(int argc, char** argv){
	GE->start();
	return 0;
}

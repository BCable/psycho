#include "Floor.hpp"

#include <curses.h>

// constructors {{{

/*! \brief Default constructor.
 */
Floor::Floor(){
}

// }}}

// inherited methods {{{

chtype Floor::charAt(int y, int x){
	char                            c;
	vector<Room*>::reverse_iterator it;
	chtype                          ret;

	ret=' ';
	for(it=rooms.rbegin(); it!=rooms.rend(); it++){
		c=(*it)->charAt(y-((*it)->getY()), x-((*it)->getX()));
		if(c!=-1){
			ret=c;
			break;
		}
	}

	return ret;
}

void Floor::init(){
	this->render();
}

void Floor::render(){
	vector<Room*>::size_type i;
	for(i=0; i<rooms.size(); i++){
		rooms[i]->render();
	}
}

bool Floor::testBlocked(int y, int x){
	vector<Room*>::size_type i;
	bool                     ret=false;

	for(i=0; i<rooms.size(); i++){
		ret=rooms[i]->testBlocked(y, x);
		if(ret)
			break;
	}

	return ret;
}


// }}}

// public methods {{{

/*! \brief Adds a Room to the Floor.
 *  \arg \c r The Room.
 */
void Floor::addRoom(Room* r){
	rooms.push_back(r);
}

// }}}

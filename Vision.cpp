#include "Vision.hpp"
#include "GameCore.hpp"

// constructors {{{

/*! \brief Default constructor.
 */
Vision::Vision(Character* c, int size, int y, int x){
	this->c=c;
	this->size=size;
	this->y=y;
	this->x=x;
}

// }}}

// private methods {{{

int Vision::getLength(){
	return size*2+1;
}

void Vision::renderDirection(short modY, short modX){
	renderDirectionFrom(this->y, this->x, modY, modX);
}

void Vision::renderDirectionFrom(
	int startY, int startX, short modY, short modX
){
	int i;
	int curX;
	int curY;
	for(i=1; i<=size; i++){
		curY=startY+(modY*i);
		curX=startX+(modX*i);

		if(testInMaxBox(curY, curX))
			renderOne(curY, curX);
		else
			break;

		if(c->e->testBlocked(curY, curX))
			break;
	}
}

void Vision::renderSubSection(
	short mod1Y, short mod1X,
	short mod2Y, short mod2X
){
	renderSubSectionFrom(y+mod1Y, x+mod1X, mod1Y, mod1X, mod2Y, mod2X);
	renderSubSectionFrom(y+mod2Y, x+mod2X, mod2Y, mod2X, mod1Y, mod1X);
}

void Vision::renderSubSectionFrom(
	int y, int x, short mod1Y, short mod1X, short mod2Y, short mod2X
){
	int  crossX;
	int  crossY;
	int  nextX;
	int  nextY;
	bool test;

	test=c->e->testBlocked(y, x);
	renderOne(y, x);

	if(!test){
		renderDirectionFrom(y, x, mod1Y, mod1X);

		crossY=y+mod2Y;
		crossX=x+mod2X;

		if(testInMaxBox(crossY, crossX)){
			renderOne(crossY, crossX);

			if(!c->e->testBlocked(crossY, crossX)){
				nextY=crossY+mod1Y;
				nextX=crossX+mod1X;

				if(testInMaxBox(nextY, nextX) && testInMaxBox(nextY, nextX))
					renderSubSectionFrom(
						nextY, nextX, mod1Y, mod1X, mod2Y, mod2X
					);
			}
		}
	}
}

bool Vision::renderOne(int y, int x){
	chtype chr;
	chr=c->e->charAt(y, x);

	if(chr==' ')
		mvaddch(y, x, VISION_CHAR);
	else{
		mvaddch(y, x, chr);
		c->getMemory()->recordEntry(y, x);
	}

	return c->e->testBlocked(y, x);
}

bool Vision::testInMaxBox(int y, int x){
	if(y<this->y-size)
		return false;
	if(y>this->y+size)
		return false;
	if(x<this->x-size)
		return false;
	if(x>this->x+size)
		return false;
	return true;
}

// }}}

// inherited methods {{{

void Vision::init(){
	render();
}

void Vision::render(){
	// up left
	renderSubSection(
		-1, 0,
		-1, -1
	);

	// up right
	renderSubSection(
		-1, 0,
		-1, 1
	);

	// right up
	renderSubSection(
		0, 1,
		-1, 1
	);

	// right down
	renderSubSection(
		0, 1,
		1, 1
	);

	// down right
	renderSubSection(
		1, 0,
		1, 1
	);

	// down left
	renderSubSection(
		1, 0,
		1, -1
	);

	// left down
	renderSubSection(
		0, -1,
		1, -1
	);
	
	// left up
	renderSubSection(
		0, -1,
		-1, -1
	);

	refresh();
}

// }}}

// public methods {{{

/*! \brief Clears the visual field.
 */
void Vision::clear(){
	int i;
	int j;

	for(i=0; i<getLength(); i++){
		for(j=0; j<getLength(); j++){
			if(mvinch(y+i-size, x+j-size)==VISION_CHAR)
				mvaddch(y+i-size, x+j-size, ' ');
		}
	}

	refresh();
}

/*! \brief Gets the size of the vision.
 *  \return The size.
 */
int Vision::getSize(){
	return size;
}

/*! \brief Gets the ncurses WINDOW.
 *  \return The ncurses WINDOW.
 */
WINDOW* Vision::getWindow(){
	return w;
}

/*! \brief Gets the X value.
 *  \return The X value.
 */
int Vision::getX(){
	return x;
}

/*! \brief Gets the Y value.
 *  \return The Y value.
 */
int Vision::getY(){
	return y;
}

/*! \brief Moves the vision.
 *  \arg \c y The new Y value.
 *  \arg \c x The new X value.
 */
void Vision::move(int y, int x){
	clear();
	this->y=y;
	this->x=x;
	render();
}

// }}}

#ifndef _psycho_vision_hpp_
#define _psycho_vision_hpp_

#include <curses.h>

#include "Character.hpp"

#define VISION_CHAR (chtype) ('.' | GC->palette->BLUE)

/*! \brief Keeps track of the visual field of a Character.
 */

class Vision {
	private:
		Character* c;
		int        size;
		WINDOW*    w;
		int        x;
		int        y;

		int  getLength();
		void renderDirection(short, short);
		void renderDirectionFrom(int, int, short, short);
		void renderSubSection(short, short, short, short);
		void renderSubSectionFrom(int, int, short, short, short, short);
		bool renderOne(int, int);
		bool testInMaxBox(int, int);

	public:
		Vision(Character*, int, int, int);

		void    clear();
		int     getSize();
		WINDOW* getWindow();
		int     getX();
		int     getY();
		void    init();
		void    move(int, int);
		void    render();
};

#endif

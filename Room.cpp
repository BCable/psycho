#include <curses.h>

#include "Room.hpp"

// constructors {{{

Room::Room(int height, int width, int y, int x, short walls){
	char border[8];

	this->height=height;
	this->width=width;
	this->y=y;
	this->x=x;
	this->walls=walls;

	// walls {{{

	if(walls & WALLS_OPEN_TOP)
		border[2]=ROOM_EMPTY;
	else
		border[2]=ROOM_HLINE;

	if(walls & WALLS_OPEN_RIGHT)
		border[1]=ROOM_EMPTY;
	else
		border[1]=ROOM_VLINE;

	if(walls & WALLS_OPEN_BOTTOM)
		border[3]=ROOM_EMPTY;
	else
		border[3]=ROOM_HLINE;

	if(walls & WALLS_OPEN_LEFT)
		border[0]=ROOM_EMPTY;
	else
		border[0]=ROOM_VLINE;

	// }}}

	// corners {{{

	if((walls & WALLS_OPEN_LEFT) && (walls & WALLS_OPEN_TOP))
		border[4]=ROOM_EMPTY;
	else if(walls & WALLS_OPEN_TOP)
		border[4]=ROOM_VLINE;
	else
		border[4]=ROOM_HLINE;

	if((walls & WALLS_OPEN_TOP) && (walls & WALLS_OPEN_RIGHT))
		border[5]=ROOM_EMPTY;
	else if(walls & WALLS_OPEN_TOP)
		border[5]=ROOM_VLINE;
	else
		border[5]=ROOM_HLINE;

	if((walls & WALLS_OPEN_RIGHT) && (walls & WALLS_OPEN_BOTTOM))
		border[6]=ROOM_EMPTY;
	else if(walls & WALLS_OPEN_BOTTOM)
		border[6]=ROOM_VLINE;
	else
		border[6]=ROOM_HLINE;

	if((walls & WALLS_OPEN_BOTTOM) && (walls & WALLS_OPEN_LEFT))
		border[7]=ROOM_EMPTY;
	else if(walls & WALLS_OPEN_BOTTOM)
		border[7]=ROOM_VLINE;
	else
		border[7]=ROOM_HLINE;

	// }}}

	w=newwin(height, width, y, x);
	wborder(w,
		border[0], border[1], border[2], border[3],
		border[4], border[5], border[6], border[7]
	);

}

// }}}

// inherited methods {{{

void Room::init(){
	// nothing to do
}

void Room::render(){
	// don't do this since the rooms are not supposed to be seen anymore
	//wrefresh(w);
}

bool Room::testBlocked(int y, int x){
	bool ret=false;

	// top wall
	if(!(walls & WALLS_OPEN_TOP)){
		if(y==this->y && x>=this->x && x<=this->x+this->width-1)
			ret=true;
	}

	// right wall
	if(!(walls & WALLS_OPEN_RIGHT)){
		if(x==this->x+this->width-1 && y>=this->y && y<=this->y+this->height-1)
			ret=true;
	}

	// bottom wall
	if(!(walls & WALLS_OPEN_BOTTOM)){
		if(y==this->y+this->height-1 && x>=this->x && x<=this->x+this->width-1)
			ret=true;
	}

	// left wall
	if(!(walls & WALLS_OPEN_LEFT)){
		if(x==this->x && y>=this->y && y<=this->y+this->height-1)
			ret=true;
	}

	return ret;
}

// }}}

// public methods {{{

/*! \brief Gets the character at a location.
 */
chtype Room::charAt(int y, int x){
	return mvwinch(w, y, x) & A_CHARTEXT;
}

/*! \brief Gets the height of the room.
 *  \return The height.
 */
int Room::getHeight(){
	return height;
}

/*! \brief Gets the width of the room.
 *  \return The width.
 */
int Room::getWidth(){
	return width;
}

/*! \brief Returns the ncurses WINDOW.
 *  \return The WINDOW.
 */
WINDOW* Room::getWindow(){
	return w;
}

/*! \brief Gets the X value.
 *  \return The X value.
 */
int Room::getX(){
	return x;
}

/*! \brief Gets the Y value.
 *  \return The Y value.
 */
int Room::getY(){
	return y;
}

// }}}

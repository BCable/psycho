#ifndef _psycho_room_hpp_
#define _psycho_room_hpp_

#include <curses.h>

#include "Renderable.hpp"

/*! \brief Defines an empty space in a Room.
 */
#define ROOM_EMPTY ' '
/*! \brief Defines a horizontal line wall in a Room.
 */
#define ROOM_HLINE '-'
/*! \brief Defines a vertical line wall in a Room.
 */
#define ROOM_VLINE '|'

/*! \brief Keeps track of a single room associated with a Floor.
 */

class Room: public Renderable {
	private:
		int     height;
		WINDOW* w;
		short   walls;
		int     width;
		int     x;
		int     y;

	public:
		Room(int, int, int, int, short);

		// inherited methods
		chtype charAt(int, int);
		void   init();
		void   render();
		bool   testBlocked(int, int);

		// wall types {{{

		/*! \brief Completely closed room.
		 */
		static const short WALLS_CLOSED=0;
		
		/*! \brief Room with open top.
		 */
		static const short WALLS_OPEN_TOP=1;

		/*! \brief Room with open right side.
		 */
		static const short WALLS_OPEN_RIGHT=2;

		/*! \brief Room with open bottom.
		 */
		static const short WALLS_OPEN_BOTTOM=4;

		/*! \brief Room with open left side.
		 */
		static const short WALLS_OPEN_LEFT=8;

		// }}}

		int     getHeight();
		int     getWidth();
		WINDOW* getWindow();
		int     getX();
		int     getY();
};

#endif

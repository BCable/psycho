#include "Coords.hpp"

// constructors {{{

Coords::Coords(int y, int x){
	this->y=y;
	this->x=x;
}

// }}}

// public methods {{{

/*! \brief Gets the X value.
 *  \return The X value.
 */
int Coords::getX(){
	return x;
}

/*! \brief Gets the Y value.
 *  \return The Y value.
 */
int Coords::getY(){
	return y;
}

// }}}

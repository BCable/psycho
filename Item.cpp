#include "Item.hpp"

// constructors {{{

/*! \brief Default constructor.
 *  \arg \c c Display character associated with the item.
 *  \arg \c block If the item will block movement and vision.
 */
Item::Item(chtype c, bool block){
	this->chr=c;
	this->block=block;
}

// }}}

// inherited methods {{{

chtype Item::getChar(){
	return chr;
}

bool Item::testBlocked(){
	return block;
}

// }}}

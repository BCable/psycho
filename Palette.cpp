#include "Palette.hpp"

#include <curses.h>

// constructors {{{

/*! \brief Default constructor.
 */
Palette::Palette(){
	init_pair(PAIR_STD, -1, -1);
	init_pair(PAIR_RED, COLOR_RED, -1);
	init_pair(PAIR_GREEN, COLOR_GREEN, -1);
	init_pair(PAIR_YELLOW, COLOR_YELLOW, -1);
	init_pair(PAIR_BLUE, COLOR_BLUE, -1);
	init_pair(PAIR_MAGENTA, COLOR_MAGENTA, -1);
	init_pair(PAIR_CYAN, COLOR_CYAN, -1);

}

// }}}

// private attributes {{{

const short Palette::PAIR_STD=0;
const short Palette::PAIR_RED=1;
const short Palette::PAIR_GREEN=2;
const short Palette::PAIR_YELLOW=3;
const short Palette::PAIR_BLUE=4;
const short Palette::PAIR_MAGENTA=5;
const short Palette::PAIR_CYAN=5;

// }}}

// public attributes {{{

/*! \brief The color red.
 */
const int Palette::RED=COLOR_PAIR(Palette::PAIR_RED);

/*! \brief The color green.
 */
const int Palette::GREEN=COLOR_PAIR(Palette::PAIR_GREEN);

/*! \brief The color yellow.
 */
const int Palette::YELLOW=COLOR_PAIR(Palette::PAIR_YELLOW);

/*! \brief The color blue.
 */
const int Palette::BLUE=COLOR_PAIR(Palette::PAIR_BLUE);

/*! \brief The color magenta.
 */
const int Palette::MAGENTA=COLOR_PAIR(Palette::PAIR_MAGENTA);

/*! \brief The color cyan.
 */
const int Palette::CYAN=COLOR_PAIR(Palette::PAIR_CYAN);

/*! \brief Standout style.
 */
const int Palette::STANDOUT=A_STANDOUT;

/*! \brief Underlined style.
 */
const int Palette::UNDERLINE=A_UNDERLINE;

/*! \brief Reverse style.
 */
const int Palette::REVERSE=A_REVERSE;

/*! \brief Blink style.
 */
const int Palette::BLINK=A_BLINK;

/*! \brief Dim style.
 */
const int Palette::DIM=A_DIM;

/*! \brief Bold style.
 */
const int Palette::BOLD=A_BOLD;

// }}}

#ifndef _psycho_inventory_hpp_
#define _psycho_inventory_hpp_

#include <list>

#include "Item.hpp"

using namespace std;

/*! \brief Stores Item objects for a Character.
 */

class Inventory {
	private:
		list<Item*> items;

	public:
		Inventory();

		list<Item*> getItems();
		void        storeItem(Item*);

};

#endif

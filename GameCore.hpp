#ifndef _psycho_gamecore_hpp_
#define _psycho_gamecore_hpp_

/*! \brief Gets the global GameCore object.
 */
#define GC GameCore::getInstance()

#include "Palette.hpp"

/*! \brief Stores core display and code stuff.
 */

class GameCore {
	private:
		static GameCore* gcInstance;

	public:
		GameCore();

		Palette* palette;

		static GameCore* getInstance();
};

#endif

#include "Display.hpp"

#include "GameCore.hpp"
// constructors {{{

Display::Display(){
	coords=new list<Coords*>;
	chars=new list<chtype>;
}

// }}}

// public methods {{{

void Display::purge(){
	list<Coords*>::iterator itCo;
	list<chtype>::iterator  itCh;

	itCo=coords->begin();
	itCh=chars->begin();

	while(itCo!=coords->end()){
		if(mvinch((*itCo)->getY(), (*itCo)->getX())==*itCh)
			mvaddch((*itCo)->getY(), (*itCo)->getX(), *itCh);

		itCh++;
		itCo++;
	}

	delete coords;
	delete chars;

	coords=new list<Coords*>;
	chars=new list<chtype>;
}

void Display::put(int y, int x, chtype c){
	coords->push_back(new Coords(y, x));
	chars->push_back(c);
}

// }}}

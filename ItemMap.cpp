#include "ItemMap.hpp"
#include <curses.h>

// constructors {{{

ItemMap::ItemMap(){
}

// }}}

// private methods {{{

ItemMap::rsItem ItemMap::rsItemAt(int y, int x){
	list<Coords*>::iterator itC;
	list<Item*>::iterator   itI;
	ItemMap::rsItem         ret;

	ret.coords=NULL;
	ret.item=NULL;

	itC=coords.begin();
	itI=items.begin();

	while(itC!=coords.end() && itI!=items.end()){
		if((*itC)->getY()==y && (*itC)->getX()==x){
			ret.coords=*itC;
			ret.item=*itI;
			break;
		}

		itC++;
		itI++;
	}

	return ret;
}

// }}}

// public methods {{{

/*! \brief Retrieves the Item located at a specific location.
 *  \arg \c y The Y value.
 *  \arg \c x The X value.
 *  \return The Item.
 */
Item* ItemMap::itemAt(int y, int x){
	ItemMap::rsItem rs;
	rs=rsItemAt(y, x);
	return rs.item;
}

/*! \brief Places an Item at a specific location.
 *  \arg \c i The Item.
 *  \arg \c y The Y value.
 *  \arg \c x The X value.
 */
void ItemMap::placeItem(Item* i, int y, int x){
	coords.push_back(new Coords(y, x));
	items.push_back(i);
}

/*! \brief Takes an item at a specific location.
 *  \arg \c y The Y value.
 *  \arg \c x The X value.
 *  \return The Item taken.
 */
Item* ItemMap::takeItemAt(int y, int x){
	ItemMap::rsItem rs;
	rs=rsItemAt(y, x);

	coords.remove(rs.coords);
	items.remove(rs.item);

	return rs.item;
}

// }}}

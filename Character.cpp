#include "Character.hpp"
#include "GameEngine.hpp"

// constructors {{{

/*! \brief Default constructor.
 *  \arg \c y The Y value to place the Character at.
 *  \arg \c x The X value to place the Character at.
 */
Character::Character(int y, int x){
	this->e=GE->getCurEnviron();
	this->i=new Inventory;
	this->y=y;
	this->x=x;

	m=new Memory(35);

	v=new Vision(this, 3, y, x);
	v->init();
}

// }}}

// public methods {{{

/*! \brief Gets the Environment.
 *  \return The Environment.
 */
Environment* Character::getEnviron(){
	return e;
}

/*! \brief Gets the Memory.
 *  \return The Memory.
 */
Memory* Character::getMemory(){
	return m;
}

/*! \brief Gets the ncurses WINDOW.
 *  \return The WINDOW.
 */
WINDOW* Character::getWindow(){
	return w;
}

/*! \brief Gets the X value.
 *  \return The X value.
 */
int Character::getX(){
	return x;
}

/*! \brief Gets the Y value.
 *  \return The Y value.
 */
int Character::getY(){
	return y;
}

/*! \brief Give this Character an Item.
 *  \arg \c i The Item.
 */
void Character::giveItem(Item* i){
	this->i->storeItem(i);
}

/*! \brief Initialize the Character with ncurses.
 */
void Character::init(){
	w=newwin(1, 1, y, x);
	waddch(w, '@');
	render();
}

/*! \brief Move this character to a specific location.
 *  \arg \c y The Y value.
 *  \arg \c x The X value.
 */
void Character::move(int y, int x){
	v->move(y, x);

	mvwin(w, y, x);
	waddch(w, '@');
	render();

	this->y=y;
	this->x=x;
}

/*! \brief Render this Character.
 */
void Character::render(){
	wrefresh(w);
}

/*! \brief Notify the Character that a turn has been taken.
 */
void Character::takeTurn(){
	m->takeTurn();
}

// }}}

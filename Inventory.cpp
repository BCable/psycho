#include "Inventory.hpp"

// constructor {{{

/*! \brief Default constructor.
 */
Inventory::Inventory(){
}

// }}}

// public methods {{{

/*! \brief Gets the Item objects in the inventory.
 *  \return List of Item objects.
 */
list<Item*> Inventory::getItems(){
	return items;
}

/*! \brief Stores an item into the inventory.
 *  \arg \c i The Item to store.
 */
void Inventory::storeItem(Item* i){
	items.push_back(i);
}

// }}}

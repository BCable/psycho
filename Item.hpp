#ifndef _psycho_item_hpp_
#define _psycho_item_hpp_

#include <curses.h>

#include "Thing.hpp"

/*! \brief Describes a single character item that can be picked up and stored in
 *         a Character's Inventory.
 */

class Item: public Thing {
	private:
		bool   block;
		chtype chr;

	public:
		Item(chtype, bool);

		chtype getChar();
		bool   testBlocked();
};

#endif

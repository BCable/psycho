#ifndef _psycho_thing_hpp_
#define _psycho_thing_hpp_

#include <curses.h>

/*! \brief Describes an object that can be seen and might be solid to a
 *         Character.
 */

class Thing {
	private:
	public:

		/*! \brief Gets the character associated with the item.
		 *  \return The character.
		 */
		virtual chtype getChar()=0;

		/*! \brief Returns if the item will block.
		 *  \return If it blocks.
		 */
		virtual bool testBlocked()=0;
};

#endif

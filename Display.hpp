#ifndef _psycho_display_hpp_
#define _psycho_display_hpp_

#include <curses.h>

#include <list>

#include "Coords.hpp"

using namespace std;

class Display {
	private:
		list<Coords*>* coords;
		list<chtype>*  chars;
	public:
		Display();

		void purge();
		void put(int, int, chtype);
};

#endif

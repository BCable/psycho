#ifndef _psycho_gameengine_hpp_
#define _psycho_gameengine_hpp_

/*! \brief Gets the global GameEngine object.
 */
#define GE GameEngine::getInstance()

#include "Environment.hpp"

class Character;

/*! \brief Main execution code and ncurses handler.
 */

class GameEngine {
	private:
		Character*         c;
		Environment*       e;
		static GameEngine* geInstance;

		void ncurses_init();

	public:
		// constructors
		GameEngine();

		// singleton methods
		static GameEngine* getInstance();

		// public methods
		Character*         getCharacter();
		Environment*       getCurEnviron();
		void               init();
		void               main();
		void               start();
};

#include "Character.hpp"

#endif

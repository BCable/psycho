#ifndef _psycho_character_hpp_
#define _psycho_character_hpp_

#include <curses.h>

#include "Environment.hpp"
#include "Inventory.hpp"
#include "Item.hpp"
#include "Memory.hpp"

/*! \brief Defines a player's character attributes.
 */

class Character {
	/*! \headerfile Vision.hpp
	 */
	friend class Vision;

	private:
		Environment*  e;
		Inventory*    i;
		Memory*       m;
		class Vision* v;
		WINDOW*       w;
		int           x;
		int           y;

	public:
		Character(int, int);

		Environment* getEnviron();
		Inventory*   getInvent();
		Memory*      getMemory();
		WINDOW*      getWindow();
		int          getX();
		int          getY();
		void         giveItem(Item*);
		void         init();
		void         move(int, int);
		void         render();
		void         takeTurn();
};

#include "GameEngine.hpp"
#include "Vision.hpp"

#endif

#ifndef _psycho_memory_hpp_
#define _psycho_memory_hpp_

#include <curses.h>

#include <list>

#include "Coords.hpp"

using namespace std;

/*! \brief Serves as a Character's memory of Thing objects and walls of Floor
 *         objects seen, and forgets them over time.
 */

class Memory {
	private:
		list<short>   turnCounts;
		list<Coords*> turnMemories;
		int           turns;
		int           turnsUsed;

		bool          coordsInMemory(int, int);

	public:
		Memory(short);

		short getTurns();
		void  recordEntry(int, int);
		void  takeTurn();
};

#endif

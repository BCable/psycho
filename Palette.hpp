#ifndef _psycho_colorpalette_hpp_
#define _psycho_colorpalette_hpp_

/*! \brief Serves as an abstract palette system for ncurses.
 */

class Palette {
	private:
		static const short PAIR_STD;
		static const short PAIR_RED;
		static const short PAIR_GREEN;
		static const short PAIR_YELLOW;
		static const short PAIR_BLUE;
		static const short PAIR_MAGENTA;
		static const short PAIR_CYAN;

	public:
		Palette();

		static const int STD;
		static const int RED;
		static const int GREEN;
		static const int YELLOW;
		static const int BLUE;
		static const int MAGENTA;
		static const int CYAN;

		static const int STANDOUT;
		static const int UNDERLINE;
		static const int REVERSE;
		static const int BLINK;
		static const int DIM;
		static const int BOLD;

};

#endif
